import numpy as np

def AND(x1, x2):
    x = np.array([x1, x2])
    w = np.array([0.5, 0.5])
    b = -0.7
    tmp = np.sum(w*x) + b
    if tmp <= 0:
        return 0
    else:
        return 1 

def NAND(x1, x2):
    x = np.array([x1, x2])
    w = np.array([-0.5, -0.5])
    b = 0.7
    tmp = np.sum(w*x) + b
    if tmp <= 0:
        return 0
    else:
        return 1 

def OR(x1, x2):
    x = np.array([x1, x2])
    w = np.array([0.5, 0.5])
    b = -0.2
    tmp = np.sum(w*x) + b
    if tmp <= 0:
        return 0
    else:
        return 1 

def XOR(x1, x2):
    s1 = NAND(x1, x2)
    s2 = OR(x1, x2)
    return AND(s1, s2)

if __name__ == "__main__":
    for i in range(0, 2):
        for j in range(0, 2):
            print("AND({}, {}) = {}".format(i, j, AND(i, j)))
    print("")

    for i in range(0, 2):
        for j in range(0, 2):
            print("NAND({}, {}) = {}".format(i, j, NAND(i, j)))
    print("")

    for i in range(0, 2):
        for j in range(0, 2):
            print("OR({}, {}) = {}".format(i, j, OR(i, j)))
    print("")

    for i in range(0, 2):
        for j in range(0, 2):
            print("XOR({}, {}) = {}".format(i, j, XOR(i, j)))
    print("")
