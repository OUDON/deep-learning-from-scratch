import numpy as np
import matplotlib.pylab as plt

def step_function(x):
    return np.array(x > 0, dtype=np.int)

def sigmoid(x):
    return 1 / (1 + np.exp(-x))

def softmax(a):
    c = np.max(a)
    exp_a = np.exp(a - c)
    return exp_a / np.sum(exp_a)

def show_step_sigmoid():
    x = np.arange(-5.0, 5.0, 0.1)
    plt.plot(x, sigmoid(x))
    plt.plot(x, step_function(x))
    plt.ylim(-0.1, 1.1)
    plt.show()

a = np.array([0.3, 2.9, 4.0])
y = softmax(a)
print(y)
print(np.sum(y))
